<?php
/**
 * @file
 * Provides implementation of inline_conditions.
 */

/**
 * Implements hook_inline_conditions_info().
 */
function commerce_discount_display_condition_inline_conditions_info() {
  $conditions = array();

  $conditions['commerce_discount_display_condition_has_specified_displays'] = array(
    'label' => t('Product display'),
    'entity type' => 'commerce_line_item',
    'callbacks' => array(
      'configure' => 'commerce_discount_display_condition_has_specified_displays_configure',
      'build' => 'commerce_discount_display_condition_has_specified_displays_build',
    ),
  );
  $conditions['commerce_discount_display_condition_order_has_specified_displays'] = array(
    'label' => t('Product display'),
    'entity type' => 'commerce_order',
    'callbacks' => array(
      'configure' => 'commerce_discount_display_condition_order_has_specified_displays_configure',
      'build' => 'commerce_discount_display_condition_order_has_specified_displays_build',
    ),
  );

  return $conditions;
}

/**
 * Implements hook_inline_conditions_build_alter().
 */
function commerce_discount_display_condition_inline_conditions_build_alter(&$value) {
  switch ($value['condition_name']) {
    case 'commerce_discount_display_condition_has_specified_displays':
    case 'commerce_discount_display_condition_order_has_specified_displays':
      $displays = $value['condition_settings']['displays'];
      $value['condition_settings']['displays'] = '';
      foreach ($displays as $display) {
        $value['condition_settings']['displays'] .= reset($display);
        if ($display !== end($displays)) {
          $value['condition_settings']['displays'] .= ', ';
        }
      }
      break;
  }
}

/**
 * Configuration callback for commerce_product_has_specified_displays on product.
 *
 * @param array $settings
 *   Values for the form element.
 *
 * @return array
 *   Return a form element.
 */
function commerce_discount_display_condition_has_specified_displays_configure($settings) {
  $form = array();

  $form['displays'] = array(
    '#type' => 'textfield',
    '#title' => t('Displays'),
    '#title_display' => 'invisible',
    '#required' => TRUE,
    '#maxlength' => 4096,
    '#default_value' => commerce_discount_display_condition_configuration_default_value($settings),
    '#autocomplete_path' => 'inline_conditions/autocomplete/node/1/0',
    '#element_validate' => array('_inline_conditions_autocomplete_validate'),
    '#suffix' => '<div class="condition-instructions">' . t('The discount is active if the product has the selected display(s).') . '</div>',
  );

  return $form;
}

/**
 * Configuration callback for commerce_discount_display_condition_order_has_specified_displays on product.
 *
 * @param array $settings
 *   Values for the form element.
 *
 * @return array
 *   Return a form element.
 */
function commerce_discount_display_condition_order_has_specified_displays_configure($settings) {
  $form = array();

  $form['displays'] = array(
    '#type' => 'textfield',
    '#title' => t('Node Displays'),
    '#title_display' => 'invisible',
    '#required' => TRUE,
    '#maxlength' => 4096,
    '#default_value' => commerce_discount_display_condition_configuration_default_value($settings),
    '#autocomplete_path' => 'inline_conditions/autocomplete/node/1/0',
    '#element_validate' => array('_inline_conditions_autocomplete_validate'),
    '#suffix' => '<div class="condition-instructions">' . t('The discount is active if the order has products with the selected display(s).') . '</div>',
  );

  return $form;
}
